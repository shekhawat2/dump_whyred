#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:33903950:647ce477a4e8ad67222e1f06c9463b7741fa5b66; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:27743562:2af21ac838c8c62211d2cdc750f035cbf97ee22e EMMC:/dev/block/bootdevice/by-name/recovery 647ce477a4e8ad67222e1f06c9463b7741fa5b66 33903950 2af21ac838c8c62211d2cdc750f035cbf97ee22e:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
